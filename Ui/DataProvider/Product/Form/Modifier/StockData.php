<?php
declare(strict_types=1);

namespace Kowal\ProductSet\Ui\DataProvider\Product\Form\Modifier;

use Kowal\ProductSet\Model\Product\Type\Set;
use Magento\Bundle\Ui\DataProvider\Product\Form\Modifier\StockData as BundleStockData;

class StockData extends BundleStockData
{
    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        if ($this->locator->getProduct()->getTypeId() === Set::TYPE_CODE) {
            $config['arguments']['data']['config'] = [
                'visible' => 0,
                'imports' => [
                    'visible' => null,
                ],
            ];

            $meta['advanced_inventory_modal'] = [
                'children' => [
                    'stock_data' => [
                        'children' => [
                            'qty' => $config,
                            'container_min_qty' => $config,
                            'container_min_sale_qty' => $config,
                            'container_max_sale_qty' => $config,
                            'is_qty_decimal' => $config,
                            'is_decimal_divided' => $config,
                            'container_backorders' => $config,
                            'container_notify_stock_qty' => $config,
                        ],
                    ],
                ],
            ];
        }

        return $meta;
    }
}
