<?php
declare(strict_types=1);

namespace Kowal\ProductSet\Plugin;

use Kowal\ProductSet\Model\Product\Type\Set;
use Magento\Bundle\Helper\Catalog\Product\Configuration;
use Magento\Catalog\Model\Product\Configuration\Item\ItemInterface;

class OptionsPlugin
{
    /**
     * @param Configuration $subject
     * @param array $result
     * @param ItemInterface $item
     * @return array
     */
    public function afterGetBundleOptions(Configuration $subject, array $result, ItemInterface $item): array
    {
        if ($item->getProductType() == Set::TYPE_CODE) {
            return [];
        }

        return $result;
    }
}
