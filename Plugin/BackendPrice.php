<?php
declare(strict_types=1);

namespace Kowal\ProductSet\Plugin;

use Kowal\ProductSet\Model\Product\Type\Set;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Backend\Price;
use Magento\Framework\DataObject;

class BackendPrice
{

    /**
     * @param Price $subject
     * @param \Closure $proceed
     * @param Product|DataObject $object
     * @return bool
     */
    public function aroundValidate(
        Price    $subject,
        \Closure $proceed,
                 $object
    ) {
        if ($object instanceof Product
            && $object->getTypeId() == Set::TYPE_CODE
            && $object->getPriceType() == \Magento\Bundle\Model\Product\Price::PRICE_TYPE_DYNAMIC
        ) {
            return true;
        }

        return $proceed($object);
    }
}
