<?php
declare(strict_types=1);

namespace Kowal\ProductSet\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Kowal\ProductSet\Model\Product\Type\Set;
use Magento\Catalog\Model\Product;

class InstallProductData implements DataPatchInterface
{
    private const FIELD_LIST = [
            'price',
            'special_price',
            'special_from_date',
            'special_to_date',
            'minimal_price',
            'cost',
            'tier_price',
            'weight',
            'price_type',
            'sku_type',
            'weight_type',
            'price_view',
            'shipment_type'
        ];
    /**
     * @var ModuleDataSetupInterface $moduleDataSetup
     */
    private $moduleDataSetup;
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(ModuleDataSetupInterface $moduleDataSetup, EavSetupFactory $eavSetupFactory)
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * Do Upgrade
     *
     * @return void
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();
        $eavSetup = $this->eavSetupFactory->create(
            [
                'setup' => $this->moduleDataSetup
            ]
        );


        foreach (self::FIELD_LIST as $field) {
            $attr = $eavSetup->getAttribute(Product::ENTITY, $field, 'apply_to');
            $applyTo = explode(',', $attr);

            if (!in_array(Set::TYPE_CODE, $applyTo)) {
                $applyTo[] = Set::TYPE_CODE;
                $eavSetup->updateAttribute(
                    Product::ENTITY,
                    $field,
                    'apply_to',
                    implode(',', $applyTo)
                );
            }
        }

        $this->moduleDataSetup->endSetup();
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }
}
