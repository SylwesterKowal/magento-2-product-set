<?php
declare(strict_types=1);

namespace Kowal\ProductSet\Controller\Product;

use Kowal\ProductSet\Model\Product\Type\Set;
use Kowal\ProductSet\Model\Provider\CartProduct\AbstractProduct;
use Kowal\ProductSet\ViewModel\Product;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\SessionFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\QuoteManagement;
use Magento\Quote\Model\QuoteRepository;

class AddToCart implements HttpGetActionInterface
{
    public const SKU_PARAM = 'sku';
    /**
     * @var array
     */
    protected $productProviders;

    /**
     * @var QuoteManagement
     */
    protected $quoteManagement;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ResultFactory
     */
    private $resultFactory;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var SessionFactory
     */
    private $checkoutSessionFactory;

    /**
     * @var QuoteRepository
     */
    private $quoteRepository;

    /**
     * @param RequestInterface $request
     * @param ResultFactory $resultFactory
     * @param ProductRepositoryInterface $productRepository
     * @param SessionFactory $checkoutSessionFactory
     * @param QuoteRepository $quoteRepository
     * @param QuoteManagement $quoteManagement
     * @param array $productProviders
     */
    public function __construct(
        RequestInterface $request,
        ResultFactory $resultFactory,
        ProductRepositoryInterface $productRepository,
        SessionFactory $checkoutSessionFactory,
        QuoteRepository $quoteRepository,
        QuoteManagement $quoteManagement,
        array $productProviders = []
    ) {
        $this->request = $request;
        $this->resultFactory = $resultFactory;
        $this->productRepository = $productRepository;
        $this->checkoutSessionFactory =  $checkoutSessionFactory;
        $this->quoteRepository = $quoteRepository;
        $this->productProviders = $productProviders;
        $this->quoteManagement = $quoteManagement;
    }

    /**
     * @return ResultInterface|ResponseInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('/');
        $productSku = $this->request->getParam(self::SKU_PARAM, '');

        if (!$productSku) {
            return $resultRedirect;
        }

        try {
            $product = $this->productRepository->get($productSku);
        } catch (NoSuchEntityException $e) {
            return $resultRedirect;

        }

        $model = $this->getCartProductModel($product);

        if (!$model) {
            return  $resultRedirect;
        }

        try {
            $checkout = $this->checkoutSessionFactory->create();

            if (!$checkout->getQuoteId()) {
                $quoteId = $this->quoteManagement->createEmptyCart();
                $quote = $this->quoteRepository->get($quoteId);
                $checkout->setQuoteId($quoteId);
            } else {
                $quote = $checkout->getQuote();
            }

            $quote->addProduct($product, $model->getProductModel($product));
            $quote->setTotalsCollectedFlag(false);
            $quote->collectTotals();
            $this->quoteRepository->save($quote);
            $checkout->replaceQuote($quote);
        } catch (\Exception $e) {
            return $resultRedirect;
        }


        return $resultRedirect->setPath('checkout/cart/');
    }

    /**
     * @param ProductInterface $product
     * @return AbstractProduct|null
     */
    public function getCartProductModel(ProductInterface $product): ?AbstractProduct
    {
        foreach ($this->productProviders as $productProvider) {
            if ($productProvider instanceof AbstractProduct) {
                if ($productProvider->getTypeCode() == $product->getTypeId()) {
                    return $productProvider;
                }
            }
        }

        return null;
    }

}
