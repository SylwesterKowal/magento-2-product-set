<?php
declare(strict_types=1);

namespace Kowal\ProductSet\ViewModel;

use Magento\CatalogRule\Model\ResourceModel\Product\CollectionProcessor;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class Product implements ArgumentInterface
{
    /**
     * @var CollectionProcessor
     */
    private $catalogRuleProcessor;

    /**
     * @var FormKey
     */
    private $formKey;

    /**
     * @var \Magento\Catalog\Helper\Product
     */
    private $catalogProduct;

    /**
     * @param FormKey $formKey
     * @param \Magento\Catalog\Helper\Product $catalogProduct
     * @param CollectionProcessor $catalogRuleProcessor
     */
    public function __construct(
        FormKey $formKey,
        \Magento\Catalog\Helper\Product $catalogProduct,
        CollectionProcessor $catalogRuleProcessor
    )
    {
        $this->formKey = $formKey;
        $this->catalogProduct = $catalogProduct;
        $this->catalogRuleProcessor = $catalogRuleProcessor;
    }

    /**
     * @return FormKey
     */
    public function getFormKey(): FormKey
    {
        return $this->formKey;
    }

    /**
     * @param $product
     * @return mixed
     */
    public function getOptions($product)
    {
        $typeInstance = $product->getTypeInstance();
        $typeInstance->setStoreFilter($product->getStoreId(), $product);

        $optionCollection = $typeInstance->getOptionsCollection($product);

        $selectionCollection = $typeInstance->getSelectionsCollection(
            $typeInstance->getOptionsIds($product),
            $product
        );
        $this->catalogRuleProcessor->addPriceData($selectionCollection);
        $selectionCollection->addTierPriceData();

       return $optionCollection->appendSelections(
           $selectionCollection,
           false,
           $this->catalogProduct->getSkipSaleableCheck()
        );
    }
}
