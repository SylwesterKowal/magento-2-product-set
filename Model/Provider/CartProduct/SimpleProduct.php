<?php
namespace Kowal\ProductSet\Model\Provider\CartProduct;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\DataObject;

class SimpleProduct extends AbstractProduct
{
    public const TYPE_CODE = 'simple';

    /**
     * @param ProductInterface $product
     * @return DataObject
     */
    function getProductModel(ProductInterface $product): DataObject
    {
        $model = $this->prepareDataModel();
        $model->setData(self::PRODUCT_ID, $product->getId());
        $model->setData(self::ITEM_ID, $product->getId());

        return $model;
    }
}
