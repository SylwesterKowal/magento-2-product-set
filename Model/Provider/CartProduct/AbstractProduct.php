<?php
namespace Kowal\ProductSet\Model\Provider\CartProduct;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\DataObject;
use Magento\Framework\DataObjectFactory;

abstract class AbstractProduct
{
    public const TYPE_CODE = null;

    /**
     * @var DataObjectFactory
     */
    protected $dataObjectFactory;

    public const PRODUCT_ID ='product';
    public const ITEM_ID ='item';
    public const RELATED_PRODUCT_ID ='related_product';
    public const QTY ='qty';

    /**
     * @param DataObjectFactory $dataObjectFactory
     */
    public function __construct(DataObjectFactory $dataObjectFactory)
    {
        $this->dataObjectFactory = $dataObjectFactory;
    }

    /**
     * @return null|string
     */
    public function getTypeCode(): ?string
    {
        return static::TYPE_CODE;
    }

    /**
     * @param ProductInterface $product
     * @return DataObject
     */
    abstract function getProductModel(ProductInterface $product): DataObject;

    /**
     * @return DataObject
     */
    protected function prepareDataModel(): DataObject
    {
        return $this->dataObjectFactory->create(
            [
                static::PRODUCT_ID => null,
                static::RELATED_PRODUCT_ID => null,
                static::ITEM_ID => null,
                static::QTY => 1
            ]
        );
    }
}
