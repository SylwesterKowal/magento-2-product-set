<?php
namespace Kowal\ProductSet\Model\Provider\CartProduct;

use Kowal\ProductSet\ViewModel\Product;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\DataObject;
use Magento\Framework\DataObjectFactory;

class SetProduct extends AbstractProduct
{
    public const TYPE_CODE = 'set';
    public const BUNDLE_OPTION = 'bundle_option';
    
    /**
     * @var Product
     */
    protected $productViewModel;

    /**
     * @param DataObjectFactory $dataObjectFactory
     * @param Product $productViewModel
     */
    public function __construct(DataObjectFactory $dataObjectFactory, Product $productViewModel)
    {
        parent::__construct($dataObjectFactory);
        $this->productViewModel = $productViewModel;
    }

    /**
     * @param ProductInterface $product
     * @return DataObject
     */
    function getProductModel(ProductInterface $product): DataObject
    {
        $bundleOptions = [];
        $options = $this->productViewModel->getOptions($product);
        foreach ($options as $option) {
            $selection = $option->getSelections()[0];
            $bundleOptions[$option->getId()] = $selection->getSelectionId();
        }

        $model = $this->prepareDataModel();

        $model->setData(self::PRODUCT_ID, $product->getId());
        $model->setData(self::ITEM_ID, $product->getId());
        $model->setData(self::BUNDLE_OPTION, $bundleOptions);

        return $model;
    }
}
