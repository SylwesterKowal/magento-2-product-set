<?php
declare(strict_types=1);

namespace Kowal\ProductSet\Model;

use Kowal\ProductSet\Model\Product\Type\Set;
use Magento\Bundle\Api\Data\OptionInterface;
use Magento\Bundle\Model\Option;
use Magento\Catalog\Model\Product\Type;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;

class OptionRepository extends \Magento\Bundle\Model\OptionRepository
{
    /**
     * Retrieve product by SKU
     *
     * @param string $sku
     * @return \Magento\Catalog\Api\Data\ProductInterface
     * @throws InputException
     * @throws NoSuchEntityException
     */
    private function getProduct($sku)
    {
        $allowedProductTypes = [
            Type::TYPE_BUNDLE,
            Set::TYPE_CODE
        ];

        $product = $this->productRepository->get($sku, true, null, true);
        if (!in_array($product->getTypeId(), $allowedProductTypes)) {
            throw new InputException(__('This is implemented for bundle products only.'));
        }

        return $product;
    }

    /**
     * @inheritdoc
     */
    public function getList($sku)
    {
        $product = $this->getProduct($sku);
        return $this->getListByProduct($product);
    }

    /**
     * @inheritdoc
     */
    public function get($sku, $optionId)
    {
        $product = $this->getProduct($sku);

        /** @var Option $option */
        $option = $this->type->getOptionsCollection($product)->getItemById($optionId);
        if (!$option || !$option->getId()) {
            throw new NoSuchEntityException(
                __('The option that was requested doesn\'t exist. Verify the entity and try again.')
            );
        }

        $productLinks = $this->linkManagement->getChildren($product->getSku(), $optionId);

        /** @var OptionInterface $optionDataObject */
        $optionDataObject = $this->optionFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $optionDataObject,
            $option->getData(),
            OptionInterface::class
        );

        $optionDataObject->setOptionId($option->getId());
        $optionDataObject->setTitle($option->getTitle() === null ? $option->getDefaultTitle() : $option->getTitle());
        $optionDataObject->setSku($product->getSku());
        $optionDataObject->setProductLinks($productLinks);

        return $optionDataObject;
    }
}
