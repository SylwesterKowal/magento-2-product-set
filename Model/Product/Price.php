<?php
declare(strict_types=1);

namespace Kowal\ProductSet\Model\Product;

use Kowal\ProductSet\ViewModel\Product;
use Magento\Catalog\Api\Data\ProductTierPriceExtensionFactory;
use Magento\Customer\Api\GroupManagementInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;

class Price extends \Magento\Bundle\Model\Product\Price
{
    /**
     * @var Product
     */
    private $productViewModel;

    /**
     * @param \Magento\CatalogRule\Model\ResourceModel\RuleFactory $ruleFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param PriceCurrencyInterface $priceCurrency
     * @param GroupManagementInterface $groupManagement
     * @param \Magento\Catalog\Api\Data\ProductTierPriceInterfaceFactory $tierPriceFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Catalog\Helper\Data $catalogData
     * @param \Magento\Framework\Serialize\Serializer\Json $serializer
     * @param ProductTierPriceExtensionFactory $tierPriceExtensionFactory
     * @param Product $productViewModel
     */
    public function __construct(
        \Magento\CatalogRule\Model\ResourceModel\RuleFactory $ruleFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        PriceCurrencyInterface $priceCurrency,
        GroupManagementInterface $groupManagement,
        \Magento\Catalog\Api\Data\ProductTierPriceInterfaceFactory $tierPriceFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Catalog\Helper\Data $catalogData,
        \Magento\Framework\Serialize\Serializer\Json $serializer,
        ProductTierPriceExtensionFactory $tierPriceExtensionFactory,
        Product $productViewModel
    ) {
        parent::__construct(
            $ruleFactory,
            $storeManager,
            $localeDate,
            $customerSession,
            $eventManager,
            $priceCurrency,
            $groupManagement,
            $tierPriceFactory,
            $config,
            $catalogData,
            $serializer,
            $tierPriceExtensionFactory
        );

        $this->productViewModel = $productViewModel;
    }

    /**
     * @param $bundleProduct
     * @param $selectionProduct
     * @param $bundleQty
     * @param $selectionQty
     * @param $multiplyQty
     * @param $takeTierPrice
     * @return float|mixed
     */
    public function agetSelectionFinalTotalPrice(
        $bundleProduct,
        $selectionProduct,
        $bundleQty,
        $selectionQty,
        $multiplyQty = true,
        $takeTierPrice = true
    ) {
        if (null === $bundleQty) {
            $bundleQty = 1.;
        }
        if ($selectionQty === null) {
            $selectionQty = $selectionProduct->getSelectionQty();
        }

        if ($bundleProduct->getPriceType() == self::PRICE_TYPE_DYNAMIC) {
            $totalQty = $bundleQty * $selectionQty;
            if (!$takeTierPrice || $totalQty === 0) {
                $totalQty = 1;
            }
            $price = $selectionProduct->getFinalPrice($totalQty);
        } else {
            if ($selectionProduct->getSelectionPriceType()) {
                // percent
                $product = clone $bundleProduct;
                $product->setFinalPrice($this->getPrice($product));
                $this->_eventManager->dispatch(
                    'catalog_product_get_final_price',
                    ['product' => $product, 'qty' => $bundleQty]
                );
                $price = $product->getData('final_price') * ($selectionProduct->getSelectionPriceValue() / 100);
            } else {
                // fixed
                $price = $selectionProduct->getSelectionPriceValue();
                if ($price == 0) {
                    $price = $this->getPartialPrice($bundleProduct, $selectionProduct);
                }

            }
        }

        if ($multiplyQty) {
            $price *= $selectionQty;
        }

        return min(
            $price,
            $this->_applyTierPrice($bundleProduct, $bundleQty, $price),
            $this->_applySpecialPrice($bundleProduct, $price)
        );
    }

    private function getPartialPrice($bundleProduct, $selectedProduct)
    {
        $totalBundlePrice = $this->getCollectedPrices($bundleProduct);
        $currentBundlePrice = $bundleProduct->getPriceInfo()->getPrice('final_price')->getAmount()->getValue();

        $discount = min($currentBundlePrice / $totalBundlePrice, 1);

        return $selectedProduct->getPrice() * $discount;
    }

    private function getCollectedPrices($bundleProduct)
    {
        $price = 0;
        $options = $this->productViewModel->getOptions($bundleProduct);

        foreach ($options as $option) {
            foreach ($option->getSelections() as $selection) {
                $price += $selection->getFinalPrice();
            }
        }

        return $price;
    }
}
