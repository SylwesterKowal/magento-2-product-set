<?php
declare(strict_types=1);

namespace Kowal\ProductSet\Model\Product\Type;

use Magento\Bundle\Model\Product\Type;

class Set extends Type
{
    public const TYPE_CODE = 'set';
}
