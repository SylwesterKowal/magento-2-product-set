<?php
declare(strict_types=1);

namespace Kowal\ProductSet\Observer;

use Kowal\ProductSet\Model\Product\Type\Set;
use Kowal\ProductSet\ViewModel\Product;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Tax\Api\TaxCalculationInterface;

class PrepareSetProduct implements ObserverInterface
{

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var float[]
     */
    private $calculatedPrice = [];

    /**
     * @var TaxCalculationInterface
     */
    private TaxCalculationInterface $taxCalculation;

    /**
     * @var Product
     */
    private Product $productViewModel;

    /**
     * @param TaxCalculationInterface $taxCalculation
     * @param Product $productViewModel
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        TaxCalculationInterface $taxCalculation,
        Product $productViewModel,
        ProductRepositoryInterface $productRepository
    ) {
        $this->taxCalculation = $taxCalculation;
        $this->productViewModel = $productViewModel;
        $this->productRepository = $productRepository;
    }
    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        /**
         * @var OrderInterface $order
         */
        $order = $observer->getEvent()->getOrder();

        /**
         * @var \Magento\Sales\Model\Order\Item $item
         */
        $newItems = [];
        foreach ($order->getItems() as &$item) {
            if ($item->getProductType() == Set::TYPE_CODE) {
                //remove item from order
                continue;
            }

            $parentItem = $item->getParentItem();

            if ($parentItem && $parentItem->getProductType() == Set::TYPE_CODE) {
                $item->setData(OrderItemInterface::PARENT_ITEM, null);
                $item->setHasChildren(false);
                $options = $item->getProductOptions();
                unset($options['bundle_selection_attributes']);
                $item->setProductOptions($options);

                $this->fixItem($order, $item, $parentItem);

            }

            $newItems[] = $item;
        }

        if ($newItems) {
            $order->setItems($newItems);
        }
    }

    /**
     * @param $parentItem
     * @return int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getCalculatedPrice($parentItem)
    {
        $id = $parentItem->getProductId();
        if (!isset($this->calculatedPrice[$id])) {
            $this->calculatedPrice[$id] = 0;
            $product = $this->productRepository->getById($id);
            $opts = $this->productViewModel->getOptions($product);

            foreach ($opts as $opt) {
                $selection = $opt->getSelections()[0];
                $this->calculatedPrice[$id] += $selection->getFinalPrice() * $selection->getSelectionQty();
            }
        }

        return $this->calculatedPrice[$id] ?? 0.0;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param \Magento\Sales\Model\Order\Item $item
     * @param \Magento\Sales\Model\Order\Item $parentItem
     * @return void
     */
    public function fixItem($order, &$item, &$parentItem)
    {
        $calculatedPrice = $this->getCalculatedPrice($parentItem);

        $product = $item->getProduct();
        $priceInfo = $product->getPriceInfo()->getPrice('final_price');
        $basePriceOld = $priceInfo->getAmount()->getBaseAmount();
        $finalPriceOld = $priceInfo->getAmount()->getValue();

        $discount = $this->getDiscount($parentItem->getOriginalPrice(), $calculatedPrice);
        $basePrice = $basePriceOld * $discount;

        $qty = $item->getQtyOrdered();

        $discountAmount = ($finalPriceOld - ($finalPriceOld * $discount)) * $qty;
        $priceAfterDiscount = $finalPriceOld * $qty - $discountAmount;
        $newDiscount = $this->getDiscountAmount($priceAfterDiscount, $discountAmount, $parentItem);

        $finalPrice = $finalPriceOld - $newDiscount;
        $taxAmount = $finalPrice - $basePrice;

        $item->setBasePrice($basePrice);
        $item->setOriginalPrice($finalPrice);
        $item->setPrice($basePrice);
        $item->setPriceInclTax($finalPrice);
        $item->setBasePriceInclTax($finalPrice);
        $item->setBaseRowTotal($qty * $basePrice);
        $item->setRowTotal($qty * $basePrice);
        $item->setRowTotalInclTax($qty * $finalPrice);

        $rate = $this->taxCalculation->getCalculatedRate(
            $product->getTaxClassId(),
            $order->getCustomerId(),
            $order->getStoreId()
        );

        $item->setTaxPercent($rate);
        $item->setTaxAmount($taxAmount * $qty);
        $item->setBaseTaxAmount($taxAmount * $qty);
    }

    /**
     * @param $requestedDiscount
     * @param $parentItem
     * @return mixed
     */
    private function getDiscountAmount($priceAfterDiscount, $requestedDiscount, &$parentItem)
    {
        $totalPrice = $parentItem->getRowTotalInclTax();
        $diff = $totalPrice - $priceAfterDiscount;

        if ($diff > 0) {
            $parentItem->setRowTotalInclTax($totalPrice - $priceAfterDiscount);
            return $requestedDiscount;
        }

        return $requestedDiscount - $diff;
    }

    /**
     * @param $currentPrice
     * @param $setProductPrice
     * @return float
     */
    private function getDiscount($currentPrice, $setProductPrice)
    {
        $percent = $currentPrice / max($setProductPrice, 1);
        return round($percent, 10);
    }
}
